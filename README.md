# Spring Boot application used to test security scanner ( contains vulnerabilities)

A project used to test some security / quality tools.

## Vulnerabilities

### XSS
* http://localhost:8080/article?slug=%3Cscript%3Ealert(%22TEST%22);%3C/script%3E

### SQL injection
* http://localhost:8080/user?login=abc%27%20or%20%271%27=%271


###  Dependency with vulnerability
* xstream 1.4.2: http://x-stream.github.io/CVE-2017-7957.html
